<?php

namespace Laravel\Spark\Http\Controllers;

use Laravel\Spark\Spark;

use Session;
use Illuminate\Http\Request;
use App\Product;
use App\Plan;
use App\PlanCategory;

class PlanController extends Controller
{
    /**
     * Get the all of the regular plans defined for the application.
     *
     * @return Response
     */
    public function all()
    {
        return response()->json(Spark::allPlans());
    }

    public function getone(Request $request, $id){
       
        // $product= Product::where('id', $id)->get()->first(); 
       
        // $name=$product->name;
        // $text=$product->text;
        // $plan=$product->plan_id;
        // $price=$product->price;
        // $cost_price=$product->cost_price;
        // $picture=$product->picture;
       
        $plan_category_id = PlanCategory::where('uuid',$id)->pluck('id');
        $plans = Plan::where('plan_category_id',$plan_category_id)->get();

        $month_id=$plans[0]->plan_id;
        $month_name=$plans[0]->name;
        $month_reference_name=$plans[0]->reference_name;
        $month_text=$plans[0]->text;
        $month_plan=$plans[0]->plan_id;
        $month_price=$plans[0]->price;
       

        $yearly_id=$plans[1]->plan_id;
        $yearly_name=$plans[1]->name;
        $yearly_reference_name=$plans[1]->reference_name;
        $yearly_text=$plans[1]->text;
        $yearly_plan=$plans[1]->plan_id;
        $yearly_price=$plans[1]->price;

        
        //return '[{"id":"plan_Cm49Sd2AU6YBCG","name":"Developer Edition","price":"59.00","trialDays":0,"interval":"monthly","features":[],"active":true,"attributes":[],"type":"user"}]';
        //return '[{"id":"' . $plan .'","name":"' . $name . '","text":"' . $text . '","price":"' . $price . '","cost_price":"' . $cost_price . '","trialDays":0,"interval":"monthly","features":[],"active":true,"attributes":[],"type":"user","picture":"' . $picture .' "}]';
        return '[{"id":"' . $month_id .'","name":"' . $month_name . '","reference_name":"' . $month_reference_name . '","text":"' . $month_text . '","price":"' . $month_price . '","trialDays":0,"interval":"monthly","features":[],"active":true,"attributes":[],"type":"user"}, {"id":"' . $yearly_id .'","name":"' . $yearly_name . '","reference_name":"' . $yearly_reference_name . '","text":"' . $yearly_text . '","price":"' . $yearly_price . '","trialDays":0,"interval":"monthly","features":[],"active":true,"attributes":[],"type":"user"}]';
        //return response()->json(Spark::allPlans());

    }

    public function planName(Request $request, $id){

        return  $plan_name = PlanCategory::where('uuid',$id)->pluck('title');
 
 
    }

}
