
/*
 |--------------------------------------------------------------------------
 | Laravel Spark Bootstrap
 |--------------------------------------------------------------------------
 |
 | First, we will load all of the "core" dependencies for Spark which are
 | libraries such as Vue and jQuery. This also loads the Spark helpers
 | for things such as HTTP calls, forms, and form validation errors.
 |
 | Next, we'll create the root Vue application for Spark. This will start
 | the entire application and attach it to the DOM. Of course, you may
 | customize this script as you desire and load your own components.
 |
 */

require('spark-bootstrap');

require('./components/bootstrap');

Vue.component(
  'cancel-button-component',
  require('./components/CancelButtonComponent.vue')
);

Vue.component(
  'subscribe-button-component',
  require('./components/SubscribeButtonComponent.vue')
);


var app = new Vue({
    mixins: [require('spark')]
});


//========================  disabling right click =======================
window.onload = function() {
    // document.addEventListener("contextmenu", function(e){
    //   e.preventDefault();
    // }, false);
    // document.addEventListener("keydown", function(e) {
    // //document.onkeydown = function(e) {
    //   // "I" key
    //   if (e.ctrlKey && e.shiftKey && e.keyCode == 73) {
    //     disabledEvent(e);
    //   }
    //   // "J" key
    //   if (e.ctrlKey && e.shiftKey && e.keyCode == 74) {
    //     disabledEvent(e);
    //   }
    //   // "S" key + macOS
    //   if (e.keyCode == 83 && (navigator.platform.match("Mac") ? e.metaKey : e.ctrlKey)) {
    //     disabledEvent(e);
    //   }
    //   // "U" key
    //   if (e.ctrlKey && e.keyCode == 85) {
    //     disabledEvent(e);
    //   }
    //   // "F12" key
    //   if (event.keyCode == 123) {
    //     disabledEvent(e);
    //   }
    // }, false);
    // function disabledEvent(e){
    //   if (e.stopPropagation){
    //     e.stopPropagation();
    //   } else if (window.event){
    //     window.event.cancelBubble = true;
    //   }
    //   e.preventDefault();
    //   return false;
    // }
  };
// =======================  disabling right click ended =================
