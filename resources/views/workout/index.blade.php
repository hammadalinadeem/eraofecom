@extends('master')
@section('title', 'Workout')

@section('page-title')
  @yield('title')
@endsection

@section('page-content')
          <!-- =============== training-body =============== -->
      
     
        <h4>Your Workouts</h4>

        <div class="row">  
        
          @foreach ($products as $product)
          
            <div class="col-sm-4">
                  <div class="card">
                    <img class="card-img-top" src='{{asset("storage/$product->picture")}}' alt="Card image cap">
                    <div class="card-body">
                      <h5 class="card-title">{{$product->name}}</h5>
                      <a href="workouts/{{$product->id}}" class="btn btn-primary">Start Now</a>
                    </div>
                  </div>
                </div>
          
            @endforeach

        </div>

        

        <!-- <div class="row">
            <div class="container">
              <p>You havent subscribed to any of the Licence or Designation yet. Click on the button to get started now.</p>
              <a href="settings#/subscription" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Subscribe Now</a>
            </div>
        </div> -->

      
          <!-- =============== training-body ended=============== -->
@endsection
