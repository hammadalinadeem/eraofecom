@extends('app')
@section('title', '')

@section('page-title')
  @yield('title')
@endsection

@section('page-content')

<div class="congs-wrapper">
    <div class="fluid-container bg-color-stg congs-body">
        <div class="row congs-align-stg" >
              <p class="congs-line1" > Congratulations! </p>   
        </div>

        <div class="row congs-align-stg">
              <p class="congs-line2">You've successfully subscribed to your plan. Please go to your 'Dashboard' to begin your training. </p>      
        </div>
        
        <div class="row congs-align-stg congs-bttn">
        
          <div class="cong-btn"> 
                <a class="congs-line3" href="/home"> GO TO MY DASHBOARD </a>
          </div>
    
        </div>
    
    </div>
</div>

<link rel="stylesheet" href="{{ mix('/css/thankyou.css') }}">

<script  src="{{ mix('/js/tracking-codes/thankyou.js') }}"></script>

@endsection