@extends('master')
@section('title', 'Dashboard')

@section('page-title')
  @yield('title')
@endsection


@inject('IsUnderFreeTrial', 'App\Tasks\IsUnderFreeTrial')

@section('page-content')
          <!-- =============== training-body =============== -->
        
        <h4>Active Subscriptions</h4>
        <br />
        <div class="row">
          
          @foreach ($products as $product)
             
                  <div class="col-sm-4">
                    <div class="card">
                      <img class="card-img-top" src='{{asset("storage/$product->picture")}}' alt="Card image cap">
                      <div class="card-body">
                        <h5 class="card-title">{{$product->name}}</h5>
                        <a href="{{ $IsUnderFreeTrial->verify() }}/home/{{$product->id}}" class="btn btn-primary">View Details</a>
                      </div>
                    </div>
                  </div>
         
          @endforeach   
            
        </div> <!-- row ended -->

      

        <!-- <div class="row">
            <div class="container">
              <p>You havent subscribed to any of the Licence or Designation yet. Click on the button to get started now.</p>
              <a href="settings#/subscription" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">Subscribe Now</a>
            </div>
        </div> -->

      
       
          <!-- =============== training-body ended=============== -->
@endsection
