@extends('spark::layouts.extended')

@section('content')


<home :user="user" inline-template>
    <div class="container" style="max-width:1400px">
        <!-- Application Dashboard -->
        <div class="row justify-content-center">
     
        <div class="col-md-12">
                <div class="card card-default">
                    <div class="card-header">
                      @yield('page-title')
                    </div>

                    <div class="card-body">
                        @yield('page-content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</home>

@endsection
