@extends('master')
@section('title',  'Phase ' . $questId . ' of ' . $productName )

@section('page-title')
  @yield('title')
@endsection

@section('page-content')
@push('scripts')


  <h2>Quiz</h2>
  <br />
  @if (Session::has('error'))
       <div class="alert alert-warning alert-dismissible fade show" role="alert">
         <strong>Sorry!</strong>  {{ session('error') }}
         <button type="button" class="close" data-dismiss="alert" aria-label="Close">
           <span aria-hidden="true">&times;</span>
         </button>
       </div>
   @endif

  <form action="evaluate" method="post">
    @csrf
    @method('put')


          @foreach ($questions as $question)

           
                    <div class="class">
                        <h3>Q - {{ $question->text }}</h3>
                        <div class="Question">
                          <?php $options = json_decode($question->options);?>
                          <?php foreach ( $options as $key => $value ) {  ?>
                            <div class="custom-control custom-radio">
                            <input type="radio" id="{{$question->id}}_{{$key}}" name="{{$question->id}}_inputoption" value="{{$key}}" class="custom-control-input">
                              <label class="custom-control-label" for="{{$question->id}}_{{$key}}"> {{$value}}  </label>
                            </div>
                          <?php } ?>
                        </div><!-- question -->
                        <br />
                      </div><!-- class ended -->
             
            @endforeach


    <br />
    <button type="submit" class="btn btn-primary">Submit</button>

  </form>
@endsection
