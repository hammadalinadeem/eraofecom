<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ Admin::title() }}</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/bootstrap/css/bootstrap.min.css") }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/font-awesome/css/font-awesome.min.css") }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/dist/css/skins/" . config('admin.skin') .".min.css") }}">

    {!! Admin::css() !!}
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/laravel-admin/laravel-admin.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/nprogress/nprogress.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/sweetalert/dist/sweetalert.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/nestable/nestable.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/toastr/build/toastr.min.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/bootstrap3-editable/css/bootstrap-editable.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/google-fonts/fonts.css") }}">
    <link rel="stylesheet" href="{{ asset("/vendor/laravel-admin/AdminLTE/dist/css/AdminLTE.min.css") }}">

    <!-- REQUIRED JS SCRIPTS -->
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/plugins/jQuery/jQuery-2.1.4.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/bootstrap/js/bootstrap.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/plugins/slimScroll/jquery.slimscroll.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/AdminLTE/dist/js/app.min.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/jquery-pjax/jquery.pjax.js") }}"></script>
    <script src="{{ asset ("/vendor/laravel-admin/nprogress/nprogress.js") }}"></script>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body class="hold-transition {{config('admin.skin')}} {{join(' ', config('admin.layout'))}}">
<div class="wrapper">

    @include('admin::partials.header')

    @include('admin::partials.sidebar')

    <div class="content-wrapper" id="pjax-container">
        @yield('content')
       
        <div class="row">
            <div class="col-sm-12">
                <h2>Update a Product</h2>
            </div>

            <div class="col-sm-12">
                <form role="form" method="POST" action="update" enctype="multipart/form-data"> 
                @csrf
                    <div class="box-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Name</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="Name" name="name" value="{{$product->name}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Zone</label>
                            <select class="form-control select2" style="width: 100%;" name="zone_id">
                                @foreach($zones as $zone)
                                    <option value="{{$zone->id}}" @if("{{$zone->id}}" === "{{$product->zone_id}}") selected="selected" @endif>{{$zone->name}}</option> 
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputEmail1">Checkout Page Description</label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="You will get training and more ..." name="text" value="{{$product->text}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Type</label>
                            <select class="form-control select2" style="width: 100%;" name="type">
                                <option value="licence" @if($product->type == 'licence') selected="selected" @endif >Licence</option>
                                <option value="designation" @if($product->type == 'designation') selected="selected" @endif>Designation</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Category</label>
                            <select class="form-control select2" style="width: 100%;" name="category">
                                <option value="standard" @if($product->category == 'standard') selected="selected" @endif>Standard</option>
                                <option value="premium" @if($product->category == 'premium') selected="selected" @endif>Premium</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Plan ID</label>
                            <input type="text" name="plan_id" class="form-control" id="exampleInputPassword1" placeholder="Plan ID" value="{{$product->plan_id}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Price</label>
                            <input type="text" name="price" class="form-control" id="exampleInputPassword1" placeholder="Price" value="{{$product->price}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Cost Price: The actual price that we were suppose to sell on</label>
                            <input type="text" name="cost_price" class="form-control" id="exampleInputPassword1" placeholder="Cost Price" value="{{$product->cost_price}}">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Publish</label>
                            <select class="form-control select2" style="width: 100%;" name="publish">
                                <option value="1" @if($product->publish == '1') selected="selected" @endif>Yes</option>
                                <option value="0" @if($product->publish == '0') selected="selected" @endif>No</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-3">
                                <img src="/storage/{{$product->picture}}" width="100%" height="100%"/>
                            </div>
                            <label for="exampleInputFile">Picture</label>
                            <input type="file" id="exampleInputFile" name="picture">
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>
             </div>
        </div><!-- row ended-->

        {!! Admin::script() !!}
    </div>

    @include('admin::partials.footer')

</div>

<!-- ./wrapper -->

<script>
    function LA() {}
    LA.token = "{{ csrf_token() }}";
</script>

<!-- REQUIRED JS SCRIPTS -->
<script src="{{ asset ("/vendor/laravel-admin/nestable/jquery.nestable.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/toastr/build/toastr.min.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/bootstrap3-editable/js/bootstrap-editable.min.js") }}"></script>
<script src="{{ asset ("/vendor/laravel-admin/sweetalert/dist/sweetalert.min.js") }}"></script>
{!! Admin::js() !!}
<script src="{{ asset ("/vendor/laravel-admin/laravel-admin/laravel-admin.js") }}"></script>

</body>
</html>
