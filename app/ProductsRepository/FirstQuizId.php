<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use App\Quiz;

class FirstQuizId{

    function __construct() {
       
    }


    public static function get($productId){
        $product_first_quiz_id = Quiz::where('product_id', $productId)
                                ->where('level','1.1')
                                ->pluck('id')->first();
                                 //here the return value is actually the level i.e 1.2,1.3 of the quiz id from the quiz table
          
        return $product_first_quiz_id;
    }


}



