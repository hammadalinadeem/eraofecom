<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;

class PublishedProducts{

    function __construct() {
       
    }


    public function getAllPublishedProducts(){
        
        $products =  Product::where('publish',1)->get(); 

        return $products;

       
    }

    public static function get(){

        $products =  Product::where('publish',1)->get(); 

        return $products;
        
    }


}



