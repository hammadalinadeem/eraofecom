<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Laravel\Cashier\Subscription;

class SubscribedProduct{

    function __construct() {
       
    }


   public function verify($product_id){
        
        $user_id = Auth::id();
        $product_plan_id = Product::where('id',$product_id)->pluck('plan_id');
        $current_billing_plan = Subscription::where('user_id',$user_id)
                                            ->where('ends_at',NULL)
                                            ->where('stripe_plan',$product_plan_id)
                                            ->pluck('stripe_plan');
        if(collect($current_billing_plan)->isEmpty()){
            return false;
        }else{
            return true;
        }

       
    }


}



