<?php

namespace App\ProductsRepository;

use Illuminate\Http\Request;
use App\Product;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Redirector;
use App\Quiz;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\ProductsRepository\CurrentQuizLevel;

class NextQuizId{

    function __construct() {
       
    }


    public static function get($productId){
        
        $user_id = Auth::id();

        $current_quiz_level = CurrentQuizLevel::get($productId);

        $current_quiz_level_explode = explode('.', $current_quiz_level);

        // get the next section id
        switch ($current_quiz_level_explode[1]) {
            case 4:
                $current_quiz_level += 0.7;

                break;

            case 1:
            case 2:
            case 3:

                $current_quiz_level += 0.1;

                break;
            default:
                # code...
                break;

        }

       

        $nexQuizId = Quiz::where('product_id', $productId)
            ->where('level', "$current_quiz_level")->pluck('id')->first();

        
        return $nexQuizId;
    }


}



