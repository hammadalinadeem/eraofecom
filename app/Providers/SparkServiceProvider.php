<?php

namespace App\Providers;

use App\Notifications\NewSale;
use App\Plan;
use App\Tasks\UserCountry;
use App\User;
use Carbon\Carbon;
use Laravel\Spark\Providers\AppServiceProvider as ServiceProvider;
use Laravel\Spark\Spark;
use Notification;
use Webpatser\Uuid\Uuid;
use App\Tasks\CustomUUID;

class SparkServiceProvider extends ServiceProvider
{
    /**
     * Your application and company details.
     *
     * @var array
     */
    protected $details = [
        'vendor' => 'ERAOFECOM',
        'product' => 'Your Product',
        // 'street' => 'PO Box 111',
        'location' => '228 Hamilton Ave, 3rd Flr, Palo Alto, California, 94301.USA',
        'phone' => '+1-650-503-1188',
    ];

    /**
     * The address where customer support e-mails should be sent.
     *
     * @var string
     */
    protected $sendSupportEmailsTo = null;

    /**
     * All of the application developer e-mail addresses.
     *
     * @var array
     */
    protected $developers = [
        'shahlajalalicoo@gmail.com',
        'tariqkhursheedceo@gmail.com',
        'rameezisrarcode@gmail.com',
    ];

    /**
     * Indicates if the application will expose an API.
     *
     * @var bool
     */
    protected $usesApi = false;

    /**
     * Finish configuring Spark for the application.
     *
     * @return void
     */
    public function booted()
    {
        // adding a custom uuid field in the user table
        //assing it to the UUID library
        Spark::createUsersWith(function ($request) {
            
            $uuid = CustomUUID::get();

            $user = Spark::user();

            $data = $request->all();

            // get the customer country
            $country_name = UserCountry::name();
            // get the customer country

            $user->forceFill([
                'name' => $data['name'],
                'email' => $data['email'],
                'country' => $country_name,
                'uuid' => $uuid,
                'password' => bcrypt($data['password']),
                'last_read_announcements_at' => Carbon::now(),
                'trial_ends_at' => Carbon::now()->addDays(Spark::trialDays()),
            ])->save();

            Notification::route('mail', ['osamaf143@gmail.com','rama@grel.org','sarah@globalstaging.org', 'tariq@designationhub.com', 'shahla@designationhub.com', 'qasim@designationhub.com', 'developernewton02@gmail.com', 'danny@grel.org', 'research.alpha20@gmail.com'])
                ->notify(new NewSale($user));

            return $user;
        });
        // adding a custom uuid field in the user table ended

        // Spark::useStripe()->noCardUpFront();
        Spark::useStripe();

        $plans = Plan::where('publish', 1)->get();

        foreach ($plans as $plan) {

            if ($plan->type == "monthly") {

                Spark::plan($plan->name, $plan->plan_id)
                    ->price($plan->price);

            }

        }

        foreach ($plans as $plan) {
            
            if($plan->type == "yearly"){

                Spark::plan($plan->name, $plan->plan_id)
                ->price($plan->price)
                ->yearly();
            
            }
      
        }
        
    }
}
