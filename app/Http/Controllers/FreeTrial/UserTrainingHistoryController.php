<?php

namespace App\Http\Controllers\FreeTrial;

use App\Quiz;
use App\Training;
use App\UserTrainingHistory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UserTrainingHistoryController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $correctAnswers = [];
        $questions = [];
        $receviedAnwers = [];
        $matchedQuestions = 0;
        $unMatchedQuestions = 0;
        $productId = $request->session()->get('trainingProductId', 'default');
        $questId = $request->session()->get('trainingProductQuestId', 'default');
        $correctAnswers = $request->session()->get('correctAnswers', 'default');
        $result = DB::table('trainings')->where('product_id', $productId)->value('level_quest');
        $level = json_decode($result, true);

        $totalQuestions = count($correctAnswers);

        $data = $request->all();

        foreach ($data as $key => $value) {

            if (str_contains($key, 'inputoption')) {
                $receviedAnwers[] = $value;
            }
        }
        if (count($receviedAnwers) == 5) { // if the user has answered all 5 questions
            //compare $correctAnswers with $receviedAnwers
            foreach ($correctAnswers as $key => $value) {
                if ($value == $receviedAnwers[$key]) {
                    $matchedQuestions++;
                } else {
                    $unMatchedQuestions++;
                }
            }

            $percentageScore = number_format(($matchedQuestions / $totalQuestions) * 100, 0);

            $quest_level_id = $this->product_quest_id($productId, $questId);
            if ($percentageScore >= 60) {

                if ($questId == "4.4") {
                    $this->saveQuestData($productId, $questId, $percentageScore);
                    return redirect("/free/home/" . $productId);
                    // $request->session()->flash('status', "You’ve scored $percentageScore% on Phase  $questId and have unlocked your branding. Visit 'Branding' and download your certificate.");
                    // return redirect("/training/".$productId. "/" . $quest_level_id);

                } else {

                    $this->saveQuestData($productId, $questId, $percentageScore);
                    $request->session()->flash('status', "You’ve scored $percentageScore% on Phase  $questId and have unlocked the next phase");
                    return redirect("/free/training/" . $productId . "/" . $quest_level_id);
                }

            } else {
                $request->session()->flash('error', "You Scored: $percentageScore% on the Phase $questId, you need to secure 60% to clear it!");
                return redirect("/free/training/$productId/$quest_level_id/start-quiz");
            }
        } else {
            $quest_level_id = $this->product_quest_id($productId, $questId);
            $request->session()->flash('error', "You can't proceed without answering all the questions of the phase: $questId");
            return redirect("/free/training/$productId/$quest_level_id/start-quiz");
        }

    }

    protected function saveQuestData($productId, $questId, $percentageScore)
    {

        $userIid = Auth::id(); // authenticated user_id

        // first see whehter the data exist or not
        $obj = UserTrainingHistory::where('user_id', $userIid)
            ->where('product_id', $productId)->where('quest_level', $questId)
            ->where('score', '>', 0)->get(); // this is a collection

        if ($obj->count() == 0) { // store the data only then if data hasnt been stored

            $obj2 = UserTrainingHistory::where('user_id', $userIid)
                ->where('product_id', $productId)->where('quest_level', $questId)
                ->where('video_watched', 1)->get(); // this is a collection

            //dd($obj2[0]);

            $obj2[0]->score = $percentageScore;
            $obj2[0]->save();

        }
    }

    public function hasWatched(Request $request)
    {
        
        $userIid = Auth::id(); // authenticated user_id
        $productId = $request->session()->get('trainingProductId', 'default');
        $questId = $request->session()->get('trainingProductQuestId', 'default');

        // first see whehter the data exist or not
        $obj = UserTrainingHistory::where('user_id', $userIid)
            ->where('product_id', $productId)
            ->where('quest_level', $questId)
            ->get(); // this is a collection
        //dd($obj);
        $quest_level_id = $this->product_quest_id($productId, $questId);
        if ($obj->count() == 0) { // store the data only then if data hasnt been stored

            $UserTraininghHstory = new UserTrainingHistory;
            $UserTraininghHstory->user_id = $userIid;
            $UserTraininghHstory->product_id = $productId;
            $UserTraininghHstory->quest_level = $questId;
            $UserTraininghHstory->score = 0.00;
            $UserTraininghHstory->video_watched = true;

            $UserTraininghHstory->save();
            $request->session()->flash('watched', "You've completed the first step and are ready to begin your quiz");
            return redirect("/free/training/" . $productId . "/" . $quest_level_id);

        } // no data i.e video hasben watched yet so we are storing it now for the first time
        else {
            return redirect("/free/training/$productId/$quest_level_id");
        }

    }

    protected function product_quest_id($productId, $questId)
    {
        $questId = Quiz::where('level', $questId)
            ->where('product_id', $productId) //here the return value is actually the level i.e 1.2,1.3 of the quiz id from the quiz table
            ->pluck('id');
        return $questId[0];
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\user_training_history  $user_training_history
     * @return \Illuminate\Http\Response
     */
    public function show(user_training_history $user_training_history)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\user_training_history  $user_training_history
     * @return \Illuminate\Http\Response
     */
    public function edit(user_training_history $user_training_history)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\user_training_history  $user_training_history
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, user_training_history $user_training_history)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\user_training_history  $user_training_history
     * @return \Illuminate\Http\Response
     */
    public function destroy(user_training_history $user_training_history)
    {
        //
    }
}
