<?php

namespace App\Http\Controllers;;

use Laravel\Spark\Spark;
use Illuminate\Http\Request;
use Laravel\Spark\Http\Controllers\Controller;
use Laravel\Spark\Contracts\Interactions\Subscribe;
use Laravel\Spark\Events\Subscription\SubscriptionUpdated;
use Laravel\Spark\Events\Subscription\SubscriptionCancelled;
use Laravel\Spark\Http\Requests\Settings\Subscription\UpdateSubscriptionRequest;
use Laravel\Spark\Contracts\Http\Requests\Settings\Subscription\CreateSubscriptionRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Laravel\Cashier\Subscription;
use App\Product;
use Carbon\Carbon;
use Notification;
use App\Notifications\CustomerAnotherSale;
use App\User;

class PlanController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function post (Request $request){
        
        $requested_subscription_plan = $request->plan_id;
       
        //  only if thee user has not subscribed to this product
        //$d = Auth::user()->current_billing_plan;
        
        //get the list of subscribed products of that user
        $user_id = Auth::user()->id;
        $user_subscriptions = DB::table('subscriptions')
                                ->where('user_id',$user_id)
                                ->where('ends_at',NULL)->get();
        $verified = true;
      
        foreach($user_subscriptions as $subcription){
            if($subcription->stripe_plan == $requested_subscription_plan){
                $verified = false;
            };
        }
        //dd($user_subscriptions);

        if($verified){
            $product= Product::where('plan_id', $requested_subscription_plan)->pluck('name'); 
             //dd($request->user());
            $plan = Spark::plans()->where('id', $requested_subscription_plan)->first(); // this plan id will be sent by the request route
            // dd($plan);
            Spark::interact(Subscribe::class, [
                $request->user(), $plan, false, $request->all()
            ]);
            
            $user = User::findOrFail($user_id);

            Notification::route('mail', ['sarah@globalstaging.org','tariq@designationhub.com','shahla@designationhub.com','qasim@designationhub.com','developernewton02@gmail.com','danny@grel.org','research.alpha20@gmail.com'])
            ->notify(new CustomerAnotherSale($user,$product[0]));
           
            $request->session()->flash('subscribed', "You have subscribed to the $product[0]");
            return redirect("settings#/subscription");

        } // if ended
      
    }

   

    /**
     * Create the subscription for the user.
     *
     * @param  CreateSubscriptionRequest  $request
     * @return Response
     */
    public function store(CreateSubscriptionRequest $request)
    {
       
        $plan = Spark::plans()->where('id', $request->plan)->first();

        Spark::interact(Subscribe::class, [
            $request->user(), $plan, false, $request->all()
        ]);

        return redirect("settings#/subscription");
    }

    /**
     * Update the subscription for the user.
     *
     * @param  \Laravel\Spark\Http\Requests\Settings\Subscription\UpdateSubscriptionRequest  $request
     * @return Response
     */
    public function update(UpdateSubscriptionRequest $request)
    {
        
        // $plan = Spark::plans()->where('id', $request->plan)->first();

        // Spark::interact(Subscribe::class, [
        //     $request->user(), $plan, false, $request->all()
        // ]);

        // event(new SubscriptionUpdated(
        //     $request->user()->fresh()
        // ));
    }

    /**
     * Cancel the user's subscription.
     *
     * @param  Request  $request
     * @return Response
     */
    public function destroy(Request $request)
    {
        // get the list of user subscribed products
        $user_id = Auth::user()->id;
        $user_subscriptions = DB::table('subscriptions')
                                ->where('user_id',$user_id)
                                ->where('ends_at',NULL)->get();

        $requested_subscription_plan = $request->plan_id;
      
        $product= Product::where('plan_id', $requested_subscription_plan)->pluck('name'); 
        

         //get the stripe key from the subscription table
     
         $stripe_id = Subscription::where('user_id',$user_id)
         ->where('stripe_plan',$requested_subscription_plan)
         ->where('ends_at',NULL)
         ->pluck('stripe_id');


        if($user_subscriptions->count() > 1){
            $ends_at = new Carbon('next month');
           
            $result = Subscription::where('user_id',$user_id)
                                    ->where('stripe_plan',$requested_subscription_plan)
                                    ->where('ends_at',NULL)
                                    ->get()->first();
           
            \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
            $plans = \Stripe\Plan::all();
            $subscription = \Stripe\Subscription::retrieve($result['stripe_id']);

            $subscription->cancel(['at_period_end' => true]);
            
            $subscriptionModel = Subscription::find($result['id']);
            $subscriptionModel->ends_at= $ends_at;
            //dd($subscriptionModel->ends_at);
            $subscriptionModel->save();

            $request->session()->flash('unsubscribed', "You have unsubscribed  the $product[0]");
            return redirect("settings#/subscription");
          

            // //dd($result);
            
            // // when the user has subscribed to more than one  product and cancelling a specific product prododuct
            // $data1[] = $request->user()->subscription();
       
            
            // $data1[0]['id']="141";
            // $data1[0]['stripe_plan'] = $requested_subscription_plan; // get the request plan id
            // $data1[0]['stripe_id'] = $stripe_id[0]; // get the stripe id from the subscription table for that spefici user and stripe plan
            // $request->merge($data1);
            

            // //$request->merge($result);
            //    $updatedSubscriptiondata = $request->user()->subscription();
            //   // dd($updatedSubscriptiondata->id);
            // //dd($request->user()->subscription());
            


            // $request->user()->subscription()->cancel();
            
            // $request->session()->flash('unsubscribed', "You have unsubscribed  the $product[0]");
            // return redirect("settings#/subscription");
            
            event(new SubscriptionCancelled($request->user()->fresh()));

        }else{ 
            
            // when the user has subscribed to only one product and cancelling that prododuct



                                
            $data1[] = $request->user()->subscription();
        
    
        //  $data1[0]['current_billing_plan'] = 'plan_ClltN2pKDBNrKJ';
            $data1[0]['stripe_plan'] = $requested_subscription_plan; // get the request plan id
            $data1[0]['stripe_id'] = $stripe_id[0]; // get the stripe id from the subscription table for that spefici user and stripe plan

            //dd($data1[0]['stripe_id']);
            $request->merge($data1);
            
            //$data1[] = $request->user()->subscription();
            //dd($data1[0]['stripstripe_plane_id']);
            
        // dd($request->user()->subscription());

            $request->user()->subscription()->cancel();
            
            $request->session()->flash('unsubscribed', "You have unsubscribed  the $product[0]");
            return redirect("settings#/subscription");
            
            event(new SubscriptionCancelled($request->user()->fresh()));


        }
    }
}
