<?php

namespace App\QuizRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\SubscriptionRepository\UserSubscriptions;
use App\Quiz;
class IsFree {

   public static function verify($productId,$questId){ // get the user's subcribed products

        $result = Quiz::where('product_id', $productId) //here the return value is actually the level i.e 1.2,1.3 of the quiz id from the quiz table
                        ->where('level',$questId)
                        ->where('free',1)
                         ->first();
     

        return $result;
    } 
    

    public static function get($questId){ // get the user's subcribed products

        $result = Quiz::where('id',$questId)
                        ->where('free',1)
                         ->exists();
        
        if($result){

            return 'true';
        }else{

            return 'false';
        }
        return $result;
    } 
    
    



}



