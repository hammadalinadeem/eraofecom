<?php

namespace App\QuizRepository;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserTrainingHistory;
use App\Product;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\DB;
use App\SubscriptionRepository\UserSubscriptions;
use App\Quiz;

class Video {

   public static function get($productId,$questId = 1.1){ // get the user's subcribed products

        // get the training questions for the specific product_id
        //$roles = DB::table('trainings')->pluck('questions', 'text');
        $result = Quiz::where('product_id',$productId)
                      ->where('level',$questId)
                      ->pluck('video_file');

        return $result[0];// return all videos

    }   


}



