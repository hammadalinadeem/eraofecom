<?php

namespace App\QuizRepository;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Passed
{
    public static $count = 0;

    public static function get($productId)
    {

        $userId = Auth::id();

        $passed_quests = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

        $data = DB::table('user_training_histories')
            ->where('user_id', $userId)
            ->where('product_id', $productId)
            ->where('score', '>=', 60.00)
            ->pluck('quest_level');

        $count = 0;

        foreach ($data as $key => $value) {

            $passed_quests[$count] = "<span class='badge badge-pill badge-success'>Passed</span>";

            $count++;
        }

        $count++;

        Passed::$count = $count;

        return $passed_quests;

    }

    public static function formatted($productId)
    {

        $userId = Auth::id();

        $passed_quests = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''];

        $data = DB::table('user_training_histories')
            ->where('user_id', $userId)
            ->where('product_id', $productId)
            ->where('score', '>=', 60.00)
            ->pluck('quest_level');

        $count = 0;

        foreach ($data as $key => $value) {

            $passed_quests[$count] = "true";

            $count++;
        }

        $count++;

        Passed::$count = $count;

        return $passed_quests;

    }

}
