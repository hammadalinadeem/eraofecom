<?php

namespace App\Admin\Controllers;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use App\Quiz;
use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use App\CourseCategory;

class CourseCategoryController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {   
     
        return Admin::content(function (Content $content) {

            $content->header('Course Categories');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        // return Admin::content(function (Content $content) use ($id) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form()->edit($id));
        // });

        $disease_category = CourseCategory::where('id', $id)->get()->first();
       
        return view('vendor/admin/coursecategories.edit', [
            'disease_category' => $disease_category,
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        
        return view('vendor/admin/coursecategories.create', [

        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(CourseCategory::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->name('Name');
            $grid->created_at();
            $grid->updated_at();
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Tool::class, function (Form $form) {

            $form->display('id', 'ID');
            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request)
    {

        $designationCategory = new CourseCategory;
        $designationCategory->name = $request->name;
        $designationCategory->publish = $request->publish;
        $designationCategory->save();
        return redirect('/admin/mamango=1/auth/course_categories');

    }

    public function update(Request $request, $id)
    {

        $designationCategory = CourseCategory::find($id);
        $designationCategory->name = $request->name;
        $designationCategory->publish = $request->publish;
        $designationCategory->save();
        return redirect('/admin/mamango=1/auth/course_categories');

    }

    public function destroy($id)
    {
        // WorkoutCategory::destroy($id);

        // //also delete from the workout_scripts table -- also unlink audio
        // $deletedRows = WorkoutScript::where('workout_category_id', $id)->delete();

        // // also delete every row from the product_workout_categories table
        // $deletedRows = ProductWorkoutCategory::where('workout_category_id', $id)->delete();
        // return 'success';
    }
}
