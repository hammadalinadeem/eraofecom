<?php

namespace App\Admin\Controllers;

use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Illuminate\Http\Request;
use App\PlanCategory;
use App\Plan;
use App\Product;

class PlanController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        
        return Admin::content(function (Content $content) {

            $content->header('Plans');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        
        $plan= Plan::where('id', $id)->get(); 

        $plan_categories = PlanCategory::get();

        return view('vendor/admin/plans.edit',[
            'plan' => $plan[0],
            'plan_categories' => $plan_categories
            
        ]);

    }

    /**
     * Create interface.
     *
     * @return Content
     */
    public function create()
    {
        // return Admin::content(function (Content $content) {

        //     $content->header('header');
        //     $content->description('description');

        //     $content->body($this->form());
        // });
        
        $products = Product::where('publish',1)->get();
        
        $plan_categories = PlanCategory::get();

        return view('vendor/admin/plans.create',[
            'products' => $products,
            'plan_categories' => $plan_categories
        ]);

    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Plan::class, function (Grid $grid) {

            $grid->id('ID')->sortable();
            $grid->name('name');
            $grid->price('price');
            $grid->type('type');
            $grid->plan_id('plan_id');
            $grid->plan_category_id()->display(function($plan_category_id) {
                return PlanCategory::find($plan_category_id)->title;
            });
            $grid->publish('publish');
            $grid->created_at();
            $grid->updated_at();
            

            $grid->filter(function ($filter) {

                // Sets the range query for the created_at field
                $filter->between('created_at', 'Created Time')->datetime();

            });


            $grid->filter(function ($filter) {

               
                $filter->like('name','Name');

                $filter->like('type','type');
            });

        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Product::class, function (Form $form) {

            $form->display('id', 'ID');

            $form->text('name', 'Name');
            $form->display('created_at', 'Created At');
            $form->display('updated_at', 'Updated At');
        });
    }

    public function store(Request $request){

        $plan = new Plan;
        $plan->name = $request->name;
        $plan->reference_name = $request->reference_name;
        $plan->text = $request->text;
        $plan->type = $request->type;
        $plan->includes = $request->includes;
        $plan->plan_category_id = $request->plan_category_id;
        $plan->plan_id = $request->plan_id;
        $plan->price = $request->price;
        $plan->cost_price = $request->cost_price;
        $plan->trial_days = $request->trial_days;
        $plan->publish = $request->publish;
        $plan->save();
            
        return redirect('/admin/mamango=1/auth/plans');
     
    }

    public function update(Request $request, $id){
        
        $plan = Plan::find($id);
        $plan->name = $request->name;
        $plan->reference_name = $request->reference_name;
        $plan->text = $request->text;
        $plan->type = $request->type;
        $plan->includes = $request->includes;
        $plan->plan_category_id = $request->plan_category_id;
        $plan->plan_id = $request->plan_id;
        $plan->price = $request->price;
        $plan->cost_price = $request->cost_price;
        $plan->trial_days = $request->trial_days;
        $plan->publish = $request->publish;
        $plan->save();

        return redirect('/admin/mamango=1/auth/plans');
   
    }

    public function destroy($id){
        return false; // so the product wont be able to delte from the admin
                     // if you need to make a product delte, just remove this destroy function from here.
    }
}
