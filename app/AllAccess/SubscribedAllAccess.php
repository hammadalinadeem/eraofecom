<?php

namespace App\AllAccess;

use Illuminate\Http\Request;
use App\Product;
use App\UserTrainingHistory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Routing\Redirector;
use Laravel\Cashier\Subscription;

class SubscribedAllAccess{

    function __construct() {
       
    }

    //check whether the user has subscribed 'all access, if yes then return all the published products' 
    //return false if not subscribed to allAccess
    //else return all the published products
    public function getAllPoductsOnAccessAll(){
        
        // $user_id = Auth::id();
        // $plan_id = Product::where('id','23')->pluck('plan_id')->first(); //  $id=23 will be the same for all access product on Production Database
       

       
        // $current_billing_plans =  Subscription::where('user_id',$user_id)
        //                                         ->where('stripe_plan',$plan_id) // hardcoded all access stripe plan
        //                                         ->where('ends_at',NULL)
        //                                         ->pluck('stripe_plan');
        
        if(Auth::user()->current_billing_plan == null){

        }else{
          
           
                $subscribed_products=[];
                
                $products = Product::where('publish',1)
                                    ->get();
                

                foreach ($products as $product) {
                
                    $result = $product;
                    $subscribed_products[] = $result;
                

                }
                

            return $subscribed_products;
            

        }
    }


}



